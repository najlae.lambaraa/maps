//
//  Route.swift
//  Maps
//
//  Created by Najlae on 18/12/2023.
//

import Foundation

struct Route: Codable {
    let overviewPolyline: Polyline?

    enum CodingKeys: String, CodingKey {
        case overviewPolyline = "overview_polyline"
    }
}
