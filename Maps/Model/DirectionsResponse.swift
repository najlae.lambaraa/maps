//
//  DirectionsResponse.swift
//  Maps
//
//  Created by Najlae on 18/12/2023.
//

import Foundation

struct DirectionsResponse: Codable {
    let routes: [Route]

    enum CodingKeys: String, CodingKey {
        case routes
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        routes = try container.decode([Route].self, forKey: .routes)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(routes, forKey: .routes)
    }
}
