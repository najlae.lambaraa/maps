//
//  Polyline.swift
//  Maps
//
//  Created by Najlae on 18/12/2023.
//

import Foundation

struct Polyline: Codable {
    let points: String

    enum CodingKeys: String, CodingKey {
        case points
    }
}
