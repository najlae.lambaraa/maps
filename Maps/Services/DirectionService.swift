//
//  DirectionService.swift
//  Maps
//
//  Created by Najlae on 18/12/2023.
//
import Foundation
import GoogleMaps

class DirectionsService {
    private let apiKey: String

        init() {
            self.apiKey = Config.googleMapsAPIKey
        }

      

    func getDirections(startAddress: String, destinationAddress: String, completion: @escaping (Result<GMSPolyline, Error>) -> Void) {
            let baseUrl = "https://maps.googleapis.com/maps/api/directions/json"

            guard let origin = startAddress.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                  let destination = destinationAddress.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
                completion(.failure(NSError(domain: "Invalid URL", code: 0, userInfo: nil)))
                return
            }

            let urlString = "\(baseUrl)?origin=\(origin)&destination=\(destination)&mode=walking&key=\(apiKey)"

            guard let url = URL(string: urlString) else {
                DispatchQueue.main.async {
                    completion(.failure(NSError(domain: "Invalid URL", code: 0, userInfo: nil)))
                }
                return
            }

            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    DispatchQueue.main.async {
                        completion(.failure(error))
                    }
                    return
                }

                guard let data = data else {
                    DispatchQueue.main.async {
                        completion(.failure(NSError(domain: "No data", code: 0, userInfo: nil)))
                    }
                    return
                }

                do {
                    let decoder = JSONDecoder()
                    let directionsResponse = try decoder.decode(DirectionsResponse.self, from: data)

                    guard let route = directionsResponse.routes.first else {
                        DispatchQueue.main.async {
                            completion(.failure(NSError(domain: "No route found", code: 0, userInfo: nil)))
                        }
                        return
                    }

                    guard let overviewPolyline = route.overviewPolyline,
                          !overviewPolyline.points.isEmpty else {
                        DispatchQueue.main.async {
                            completion(.failure(NSError(domain: "Invalid polyline data", code: 0, userInfo: nil)))
                        }
                        return
                    }

                    guard let path = GMSPath(fromEncodedPath: overviewPolyline.points) else {
                        DispatchQueue.main.async {
                            completion(.failure(NSError(domain: "Error creating GMSPath", code: 0, userInfo: nil)))
                        }
                        return
                    }

                    let polyline = GMSPolyline(path: path)

                    DispatchQueue.main.async {
                        completion(.success(polyline))
                    }

                } catch let decodingError {
                   
                    DispatchQueue.main.async {
                        completion(.failure(decodingError))
                    }
                }
            }

            task.resume()
        }
    }
