//
//  Config.swift
//  Maps
//
//  Created by Najlae on 18/12/2023.
//

import Foundation
import Foundation

struct Config {
    static var googleMapsAPIKey: String {
        guard let path = Bundle.main.path(forResource: "Info", ofType: "plist"),
              let configDict = NSDictionary(contentsOfFile: path),
              let apiKey = configDict["GoogleMapsAPIKey"] as? String else {
            fatalError("Erreur de charger la clé API depuis Info.plist")
        }
        return apiKey
    }
}
