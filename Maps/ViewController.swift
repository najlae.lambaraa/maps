//
//  ViewController.swift
//  Maps
//
//  Created by Najlae on 18/12/2023.
//

import UIKit
import GoogleMaps
class ViewController: UIViewController {

    @IBOutlet weak var startAddressTextField: UITextField!
        @IBOutlet weak var destinationAddressTextField: UITextField!
        @IBOutlet weak var mapViewContainer: UIView!
        
        var mapView: GMSMapView?
        let directionsService = DirectionsService()

        override func viewDidLoad() {
            super.viewDidLoad()
            
            setupMapView()
        }

    func setupMapView() {
        DispatchQueue.main.async { [weak self] in
            guard let mapViewContainer = self?.mapViewContainer
            else {
                print("Error: mapViewContainer is nil")
                return
            }

            
            self?.mapView = GMSMapView(frame: mapViewContainer.bounds)

            guard let mapView = self?.mapView else {
                print("Error: mapView is nil")
                return
            }

            let camera = GMSCameraPosition.camera(withLatitude: 46.6035, longitude: 1.8883, zoom: 5.0)

            mapView.camera = camera

            
            mapViewContainer.addSubview(mapView)
        }
    }





        @IBAction func searchRouteButtonTapped(_ sender: UIButton) {
            print("Button tapped!")
                guard let startAddress = startAddressTextField.text, !startAddress.isEmpty,
                      let destinationAddress = destinationAddressTextField.text, !destinationAddress.isEmpty 
            else {
                    
                    showAlert(message: "Enter start and finish")
                    return
                }

                directionsService.getDirections(startAddress: startAddress, destinationAddress: destinationAddress) { [weak self] result in
                    
                    DispatchQueue.main.async {
                        
                        switch result {
                        case .success(let polyline):
                            
                            self?.displayRoute(polyline)
                        case .failure(let error):
                            
                            self?.showAlert(message: "Error getting directions: \(error.localizedDescription)")
                        }
                    }
                }
        }

    func displayRoute(_ route: GMSPolyline) {
        DispatchQueue.main.async { [weak self] in
            
            self?.mapView?.clear()

            route.map = self?.mapView
            if let path = route.path {
                var bounds = GMSCoordinateBounds()

                for index in 0 ..< path.count() {
                    bounds = bounds.includingCoordinate(path.coordinate(at: index))
                }

                self?.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
            }
        }
    }

    func showAlert(message: String) {
        DispatchQueue.main.async { [weak self] in
            let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self?.present(alert, animated: true, completion: nil)
        }
    }
    

}

